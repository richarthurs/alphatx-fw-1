/*
 * config.hpp
 *
 *  Created on: Aug 3, 2020
 *      Author: Richard
 *
 * Configuration parameters for alphaTx.
 */

#ifndef CONFIG_HPP_
#define CONFIG_HPP_

#include <string.h>

namespace config{
/**
 * @brief How many milliseconds to wait after transmitting data
 * for an ack to be received. If one is not received in this time,
 * the data is resent.
 */
#define RX_TIMEOUT_MS 1000

/**
 * @brief Size of the command buffer in bytes. This sets the maximum
 * command length including spaces and arguments.
 */
#define CMD_BUF_BYTES 64

/**
 * @brief The callsign to use when transmitting.
 */
constexpr const char* callsign = "VA7RGO";

/**
 * @brief The length of the callsign.
 */
constexpr uint32_t callsign_length = strlen(callsign);

/**
 * @brief The number of ms to wait after receiving something on the master UART
 * before transmitting it.
 */
constexpr uint32_t master_rx_timeout_ms = 1000;

}
#endif /* CONFIG_HPP_ */
