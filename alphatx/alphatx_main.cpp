/*
 * main_cpp.cpp
 *
 *  Created on: Jul. 4, 2020
 *      Author: Richard Arthurs
 */

#include "main.h"
#include "flags.hpp"
#include "radio.hpp"
#include "uart.hpp"
#include "leds.hpp"
#include "cmd.hpp"
#include "global.hpp"
#include "master-interface.hpp"

/**
 * @brief Flags.
 */
bool flag_rf_gdo0 = false;

MasterInterface debug_master_interface("debug_master", true, debug_uart);
MasterInterface master_master_interface("master_master", false, master_uart);

UART_HandleTypeDef* master_uart = nullptr;
UART_HandleTypeDef* debug_uart = nullptr;

/**
 * @brief Passes the hardware configuration pointers into atx code.
 *
 * Including atx modules into the HAL-generated main.c is difficult, so instead,
 * pass the hardware into our own code and use it from there. As a benefit, this
 * reduces the amount of HAL code that needs to be modified, increasing
 * portability.
 *
 * @param debug_uart_ptr Pointer to the debug UART instance.
 * @param master_uart_ptr Pointer to the debug UART instance.
 * @param spi_handle Pointer to the RF SPI instance.
 */
void init_alpha_main(UART_HandleTypeDef* debug_uart_ptr, UART_HandleTypeDef* master_uart_ptr, SPI_HandleTypeDef *spi_handle){
	debug_uart = debug_uart_ptr;
	master_uart = master_uart_ptr;

	atx_uart_init(debug_uart_ptr);
	radio_setup(spi_handle);
}

/**
 * @brief Main loop of the alphatx application.
 */
void alpha_loop(void) {
	while(1) {
		radio_loop();
		cmd::process_command();

		// Transmit data from either the master master interface or the debug master interface.
		if(!master_master_interface.poll_for_transmission()) {
			debug_master_interface.poll_for_transmission();
		}

		led::blink();
	}
}


