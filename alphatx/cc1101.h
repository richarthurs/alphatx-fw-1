/*
 * cc1101.h
 *
 *  Created on: Jul 1, 2020
 *      Author: Richard
 */

#ifndef CC1101_H_
#define CC1101_H_
#include "main.h"
#include "stdbool.h"

#include "stm32f1xx_hal_spi.h"

#ifdef __cplusplus
extern "C" {
#endif

void init_cc1101(SPI_HandleTypeDef *spi_handle);
bool check_partnum_and_version(void);

#ifdef __cplusplus
}
#endif


#endif /* CC1101_H_ */
