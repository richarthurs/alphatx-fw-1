/*
 * radio.hpp
 *
 *  Created on: Aug 3, 2020
 *      Author: Richard
 */

#ifndef RADIO_HPP_
#define RADIO_HPP_

#include "main.h"

bool radio_enabled(void);
void radio_setup(SPI_HandleTypeDef *spi_handle);
void radio_loop(void);
void radio_transmit(const uint8_t* data, uint16_t length);
int16_t get_last_rssi(void);

#endif /* RADIO_HPP_ */
