/*
 * utils.cpp
 *
 *  Created on: Jul 5, 2020
 *      Author: Richard
 */

#include <stdint.h>

namespace util{


/**
 * @brief Basic ascii to integer.
 *
 * @param Pointer to the string to convert.
 */
int32_t atoi(char* str) {
	int32_t res = 0;
	int32_t sign = 1;

    // Initialize index of first digit
	int32_t i = 0;

	// Track the negative sign
    if (str[0] == '-') {
        sign = -1;

        // Also update index of first digit
        i++;
    }

    // Iterate through all digits
    // and update the result
    for (; str[i] != '\0'; ++i)
        res = res * 10 + str[i] - '0';

    // Return result with sign
    return sign * res;
}

}
