/*
 * master-interface.cpp
 *
 *  Created on: January 1, 2021
 *      Author: Richard
 */

#include "master-interface.hpp"
#include "config.hpp"
#include "radio.hpp"
#include "uart.hpp"
#include "global.hpp"

/**
 * @brief Master interface that forwards data to and from the radio. 
 * 
 * The default master interface will buffer received bytes and send them with
 * the radio once no more data has been received for a period of 1 second.
 * 
 * @param name The name of this interface.
 * @param is_debug Whether or not to act in debug mode, which clears out the
 * debug UART state once data is transmitted out with the radio. This allows the
 * debug UART to act as both a debug and a master interface.
 */
MasterInterface::MasterInterface(const char* name, bool is_debug, UART_HandleTypeDef* uart) :
uart(uart),
name(name),
is_debug(is_debug),
next_char(0),
last_received_time(0) {
}

/**
 * @brief Adds a byte to the master interface.
 * @param[in] byte The byte to add to the buffer.
 * @return Whether or not the byte was added successfully.
 */
bool MasterInterface::add_byte(uint8_t byte) {
    rx_buf[next_char] = byte;
    last_received_time = HAL_GetTick();
    next_char++;

    // TODO: handle index
    return true;
}

/**
 * @brief Transmits any data in the master TX buffer if no new data has been 
 * received for a little while. This should be called in the main loop.
 * 
 * @return Whether or not the poll decided to transmit anything.
 */
bool MasterInterface::poll_for_transmission(void) {
    bool transmitted = false;
	// If we have a timeout active (meaning there's data we want to transmit
	// out over the radio), transmit it if the timer expires.
	if(last_received_time) {
		if (HAL_GetTick() - last_received_time > config::master_rx_timeout_ms) {
			radio_transmit((const uint8_t*)rx_buf, next_char);
			last_received_time = 0;
			next_char = 0;

            // On master interfaces that share with the debug UART, reset it.
            if(is_debug) {
                reset_uart();
            }
            transmitted = true;
		}
	}
    return transmitted;
}

/**
 * @brief Sends data over the UART associated with this interface.
 */
void MasterInterface::send_over_uart(uint8_t* data, uint32_t size) {
    // TODO: figure out the correct pointer initialization - maybe master/debug really just need to be distinct at this point.
    HAL_UART_Transmit(master_uart, data, size, 100);
}
