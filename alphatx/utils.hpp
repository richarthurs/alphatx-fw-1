/*
 * utils.hpp
 *
 *  Created on: Jul 5, 2020
 *      Author: Richard
 */

#ifndef UTILS_HPP_
#define UTILS_HPP_

namespace util {

int32_t atoi(char* str);

}



#endif /* UTILS_HPP_ */
