/*
 * cmd.cpp
 *
 *  Created on: Aug. 8, 2020
 *      Author: Richard
 */

#include "cmd.hpp"
#include "config.hpp"
#include "uart.hpp"
#include <string.h>
#include "stm32f1xx_hal.h"

namespace {
// Private functions.
void cmd_ack(void);
void cmd_runtime(void);
void cmd_reset(void);

/**
 * @brief States of the command system.
 */
enum class CmdState {
	IDLE,
	NEW_CMD,
	PROCESSING
};

/**
 * @brief The state of the command system.
 */
CmdState state = CmdState::IDLE;

/**
 * @brief Command buffer.
 * Raw commands are placed here once received by the UART
 * handler.
 */
uint8_t cmd_buf[CMD_BUF_BYTES];

/**
 * @brief A command.
 */
typedef struct command {
	/**
	 * @brief The string opcode for the command.
	 */
    const char* opcode;
    /**
     * @Pointer to the command's function.
     */
    void (*action)(void);
} command_t;

/**
 * @brief Command table.
 */
command_t commands[] = {
		{"ack", &cmd_ack},
		{"runtime", &cmd_runtime},
		{"reset", &cmd_reset},
};

/**
 * @brief The number of commands.
 */
constexpr uint8_t num_cmds = sizeof(commands) / sizeof(command_t);

/**
 * @brief Command that replies with Ack!
 */
void cmd_ack(void) {
	send_string("Ack!");
}

/**
 * @brief Command that replies with the number of milliseconds
 * since boot.
 */
void cmd_runtime(void) {
	send_string("Runtime: %d ms", HAL_GetTick());
}

/**
 * @brief Resets the MCU.
 */
void cmd_reset(void) {
	HAL_NVIC_SystemReset();
}
}


/**
 * @brief Indicates to the command system that there is a new command that needs to be processed.
 *
 * The data pointed to by @c cmd_str is copied into the command system and is safe to modify
 * after this function returns. No command will be registered if the input command is improper
 * or if the command system can't handle a new command. Conditions causing a command to be ignored:
 * 	- No leading /.
 * 	- Zero length.
 * 	- Too long (max length including leading / is @ref CMD_BUF_BYTES).
 * 	- Command system is not idle.
 *
 * @param[in] cmd_str Pointer to the command.
 * @param[in] len The number of bytes from cmd_str to place in the command buffer.
 */
void cmd::new_command(const char* cmd_str, uint8_t len) {
	if(len && len <= CMD_BUF_BYTES && state == CmdState::IDLE && *cmd_str == '/') {
		cmd_str++;		// Get past the / character.
		len = len - 1;	// Length includes /, remove it.

		// If the string includes \r or \n in the last two characters,
		// remove them by reducing the length to copy into the local
		// buffer.
		if(cmd_str[len] == '\r' || cmd_str[len] == '\n') {
			len--;
		}
		if(cmd_str[len] == '\r' || cmd_str[len] == '\n') {
			len--;
		}

		// Copy the sanitized string into the command buffer and indicate
		// that we have a new command to process.
		memcpy(cmd_buf, cmd_str, len);
		state = CmdState::NEW_CMD;
	}
}

/**
 * @brief Runs the command processing loop.
 * When the state hits NEW_CMD, the loop will run and execute
 * the command in the buffer if the command is valid.
 */
void cmd::process_command(void) {
	if(state == CmdState::NEW_CMD) {
		state = CmdState::PROCESSING;

		// TODO: array of commands
		void (*action)(void) = nullptr;
		for(uint8_t i = 0; i < num_cmds; i++){
			if(strncmp((const char*)cmd_buf, commands[i].opcode, CMD_BUF_BYTES) == 0) {
				action = commands[i].action;
				break;
			}
		}

		// Execute the action.
		if(action != nullptr) {
			// TODO: command timeout timer
			action();
		} else {
			send_string("Unknown command: %s", cmd_buf);
		}

		// Empty out the command buffer.
		memset(cmd_buf, 0, CMD_BUF_BYTES);
		state = CmdState::IDLE;
	}
}

