/*
 * uart.cpp
 *
 *  Created on: Jun 30, 2020
 *      Author: Richard
 */


#include "flags.hpp"
#include <string.h>
#include <stdarg.h>
#include "uart.hpp"
#include "printf/printf.h"
#include "cmd.hpp"
#include "radio.hpp"
#include "config.hpp"
#include "global.hpp"

/**
 * @brief Handle for the debug UART.
 */
UART_HandleTypeDef* _debug_uart= NULL;

/**
 * @brief The HAL timeout (ms) to use.
 */
constexpr uint32_t hal_timeout_ms = 500;

/**
 * @brief A buffer to use when formatting strings.
 */
static char buffer[100];

/**
 * @brief Received character.
 */
uint8_t rx_buf[255];

/**
 * @brief Pointer to next received character.
 */
uint8_t next_char = 0;

/**
 * @brief Received character from debug UART.
 */
uint8_t debug_rx_byte = '\0';

/**
 * @brief Received character from master UART.
 */
uint8_t master_rx_byte = '\0';

/**
 * @brief The last time we received a character from the master UART.
 */
uint32_t last_received_time = 0;

/**
 * @brief Whether or not the data currently in the RX buffer is a command or not.
 * If it's a command, it started with a /.
 */
bool is_cmd = false;


/**
 * @brief Initializes the UART.
 */
void atx_uart_init(UART_HandleTypeDef* uart){
	_debug_uart= uart;

	// Clear out buffers
	memset(rx_buf, 0, 255);

	// Set up interrupt to fire on every received character.
	next_char = 0;
	HAL_UART_Receive_IT(_debug_uart, &debug_rx_byte, 1);
	HAL_UART_Receive_IT(master_uart, &master_rx_byte, 1);
}

/**
 * @brief Processes data coming in from the UARTs, routing it to the correct
 * handler based on the UART that the data was received on.
 * 
 * This will keep track of whether we are dealing with a command or not, and
 * will send commands to the command processor once complete commands are detected.
 * 
 * Data that is not part of a command is handled in the main loop by 
 * @ref handle_transmission(). It sends any non-command data once every second,
 * if any exists.
 * 
 * @brief The UART that the data is being received on.
 */
void new_char_from_isr(UART_HandleTypeDef* uart) {
	if(uart == master_uart) {
		master_master_interface.add_byte(master_rx_byte);
		HAL_UART_Receive_IT(master_uart, &master_rx_byte, 1);
	} else {
		// Take "/" as the beginning of a command if it is the first character seen.
		if(next_char == 0 && (debug_rx_byte == '/')) {
			is_cmd = true;
		} 

		if(!is_cmd) {
			// Add to the master interface buffer
			::debug_master_interface.add_byte(debug_rx_byte);
		} else {
			// Add to the local internal command buffer
			rx_buf[next_char] = debug_rx_byte;
		}

		// If we are dealing with a command, process it once a newline is received.
		if(is_cmd && rx_buf[next_char] == '\n') {

			// Get rid of any returns in case the terminal sends \r\n.
			if(rx_buf[next_char - 1] == '\r') {
				rx_buf[next_char - 1] = '\0';
			}

			// Process as a command and reset all of the state variables.
			cmd::new_command((const char*)rx_buf, next_char);
			is_cmd = false;
			next_char = 0;
		} else {
			next_char++;
		}
		HAL_UART_Receive_IT(_debug_uart, &debug_rx_byte, 1);
	}
}

/**
 * @brief Resets the internal UART state.
 * This is used by the debug version of the master interface to indicate when it
 * has completed processing a bunch of non-command data that has come in. This
 * resets the UART so that it now will try to determine command and non-command
 * data.
 */
void reset_uart(void) {
	is_cmd = false;
	next_char = 0;	
}

/**
 * @brief Sends a string with the debug UART, adding a newline after the
 * user's string.
 *
 * @param[in] str The string to send.
 */
void send_string(const char* format, ...) {
  va_list va;
  va_start(va, format);
  size_t len = alpha_vsnprintf(buffer, 100, format, va);
  va_end(va);

	HAL_UART_Transmit(_debug_uart, (uint8_t*)buffer, len, hal_timeout_ms);
	HAL_UART_Transmit(_debug_uart, (uint8_t*)"\n", 1, hal_timeout_ms);
	memset(buffer, 0, 100);
}