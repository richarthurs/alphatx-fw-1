/*
 * flags.hpp
 *
 *  Created on: Jul 25, 2020
 *      Author: Richard
 *
 *  Global flags (defined in @ref alphatx_main.cpp)
 *  that are used to communicate between modules.
 */

#ifndef FLAGS_HPP_
#define FLAGS_HPP_

/**
 * @brief Indicates that there is new data received from the radio.
 */
extern bool flag_rf_gdo0;

#endif /* FLAGS_HPP_ */
