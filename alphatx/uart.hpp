/*
 * uart.h
 *
 *  Created on: Jun 30, 2020
 *      Author: Richard
 */

#ifndef ALPHATX_UART_H_
#define ALPHATX_UART_H_

#include "stm32f1xx_hal.h"

#ifdef __cplusplus
extern "C" {
#endif

void atx_uart_init(UART_HandleTypeDef* uart);

void new_char_from_isr(UART_HandleTypeDef* uart);
void send_string(const char* format, ...);
void handle_transmission(void);
void reset_uart(void);

#ifdef __cplusplus
}
#endif

#endif /* ALPHATX_UART_H_ */
