/*
 * cc1101.cpp
 *
 *  Created on: Jul 1, 2020
 *      Author: Richard
 */

#include "cc1101.h"

// Register read/write modes
#define BURST_READ 0xC0

// Register base addresses
#define PARTNUM 0x30	// 0x00 reset
#define VERSION 0x31 	// 0x14 reset

/**
 * @brief Handle to the SPI instance used for communication.
 */
SPI_HandleTypeDef* _spi;


/**
 * @brief Sets up the CC1101.
 * @param[in] spi_handle The SPI port to use to communicate with the CC1101.
 */
void init_cc1101(SPI_HandleTypeDef *spi_handle) {
	_spi = spi_handle;
}


/**
 * @brief Gets the part number and version from the CC1101.
 * @return True if the partnum and version are detected correctly.
 */
bool check_partnum_and_version(void) {
	uint8_t partnum = 0;
	uint8_t version = 0;

	// Full duplex SPI, so transmit a dummy byte after the read command.
	uint8_t tx[2] = {VERSION + BURST_READ, 0};
	uint8_t rx[2] = {0, 0};

	HAL_SPI_TransmitReceive(_spi, tx, rx, 2, 500);
	while(1){}
	return true;
}
