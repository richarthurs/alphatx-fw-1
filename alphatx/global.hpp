/*
 * global.hpp
 *
 *  Created on: January 1, 2021
 *      Author: Richard
 */

#ifndef ALPHATX_GLOBAL_H_
#define ALPHATX_GLOBAL_H_

#include "master-interface.hpp"

/**
 * @brief Master interface that gets data to/from the debug UART.
 */
extern MasterInterface debug_master_interface;

/**
 * @brief Master interface that gets data from the master UART.
 */
extern MasterInterface master_master_interface;

/**
 * @brief Debug UART pointer.
 */
extern UART_HandleTypeDef* debug_uart;

/**
 * @brief Master UART pointer.
 */
extern UART_HandleTypeDef* master_uart;

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* ALPHATX_GLOBAL_H_ */
