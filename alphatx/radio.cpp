/*
 * radio.cpp
 *
 *  Created on: Aug 3, 2020
 *      Author: Richard
 */

#include "radio.hpp"
#include "flags.hpp"
#include "config.hpp"
#include "main.h"
#include "stm32f1xx_hal.h"
#include "cc1101-arduino.hpp"
#include <string.h>
#include <uart.hpp>
#include "printf/printf.h"
#include "utils.hpp"
#include "global.hpp"

/**
 * @brief Transmit and receive buffers.
 */
#define TX_BUF_SIZE_BYTES 64
#define RX_BUF_SIZE_BYTES 64 + 1	// +1 so that we always end up with a null after the data (since the radio's limit is 64 bytes)
char pretransmit_buf[TX_BUF_SIZE_BYTES];
uint8_t rx_buffer[RX_BUF_SIZE_BYTES];

/**
 * @Pointer to the CC1101 instance.
 */
CC1101* radio = NULL;

/**
 * @brief The most recent RSSI value.
 */
int16_t last_rssi = -127;

/**
 * @brief States for the transmission.
 * This is used to control flow of data out of the radio.
 */
enum class TXState {
	/**
	 * @brief Ready to accept new data.
	 */
	READY,
	/**
	 * @brief We have some data and we need to transmit it.
	 */
	QUEUED,
	/**
	 * @brief We are in the process of transmitting the data.
	 */
	TRANSMITTING,
};

TXState tx_state = TXState::READY;

/**
 * @brief The size of the data that is queued for transmission.
 */
uint32_t transmit_size;

/**
 * @brief Whether or not the radio is enabled.
 */
bool enabled;

// Private functions
static void transmit(const char* data);

/**
 * @brief Sets up peripheral pointers from the HAL so they are accessible
 * from alphaTX code and initializes the CC1101.
 *
 * @note Must be called before @radio_loop.
 * @pre @ref atx_uart_init has been called.
 *
 * @param spi_handle Pointer to the RF SPI instance.
 */
void radio_setup(SPI_HandleTypeDef *spi_handle) {
	send_string("alphaTX started!");

	// Set up the radio and its buffers.
	static CC1101 the_radio(spi_handle);
	radio = &the_radio;
	// radio->setPower0dbm();
	radio->setPowerMinus30dbm();
	radio->setBaudrate38000bps();
	radio->begin(433.7e6);
	radio->setRXstate();

	// EXTI interrupt for CC1101 GDO0.
	// The radio is configured to raise this pin when a packet is
	// received.
	HAL_NVIC_SetPriority(EXTI15_10_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

	tx_state = TXState::READY;
	enabled = true;
}

/**
 * @brief Whether or not the radio is enabled.
 * @return Radio enable status.
 */
bool radio_enabled(void) {
	return enabled;
}

/**
 * @brief Transmits data with the radio.
 *
 * If we are not able to accept new data, the transmit request
 * is dropped.
 *
 * @param[in] data The data to transmit.
 * @param[in] length The number of bytes of data to transmit.
 */
void radio_transmit(const uint8_t* data, uint16_t length) {
	switch(tx_state) {
		case TXState::READY:
			memcpy(pretransmit_buf, data, length);
			transmit_size = length;
			tx_state = TXState::QUEUED;
		case TXState::QUEUED:
		case TXState::TRANSMITTING:
			// We already have something queued. We can't take any more data.
			break;
	}
}

/**
 * @brief Continuously checks if we have data to receive from the radio, and
 * grabs it if we do.
 *
 * @note Intended to be run in a while(1) loop.
 * @pre radio_setup has been called.
 */
void radio_loop(void){
	if(flag_rf_gdo0) {
		flag_rf_gdo0 = false;
		// Grab the packet
		memset(rx_buffer, 0x00, RX_BUF_SIZE_BYTES);	// Clear the RX buffer
		uint8_t packet_size = radio->getPacket(rx_buffer);
		if (packet_size > 0) {
			if (radio->crcok()) {
				send_string("Received bytes: %d %i dBm", packet_size, radio->getRSSIdbm());
				if(packet_size > config::callsign_length) {
					// We got callsign + data!
					send_string("Received: %s", rx_buffer);
					master_master_interface.send_over_uart(&rx_buffer[config::callsign_length], packet_size - config::callsign_length);
				} else {
					send_string("No data: %s", rx_buffer);
				}
			} else {
				send_string("CRC failed! %i dBm", radio->getRSSIdbm());
			}
		}
	}
	else if(tx_state == TXState::QUEUED || tx_state == TXState::TRANSMITTING) {
		transmit(pretransmit_buf);
	}
	last_rssi = radio->getRSSIdbm();
}

/**
 * @brief Gets the last RSSI from the radio.
 * @return A value between 0 (best) and -100something (worst).
 */
int16_t get_last_rssi(void) {
	return last_rssi;
}

/**
 * @brief Transmits arbitrary string data.
 */
static void transmit(const char* data) {
	static char tx_buffer[TX_BUF_SIZE_BYTES];

	// If we've got queued data, it is the first time trying to transmit it.
	// Set up the TX buffer with the data and the callsign.
	if(tx_state == TXState::QUEUED) {
		tx_state = TXState::TRANSMITTING;
		
		// Empty the TX buffer and place the callsign in it.
		memset(tx_buffer, 0, TX_BUF_SIZE_BYTES);
		alpha_sprintf(tx_buffer, config::callsign);

		// Place the data to transmit into the buffer.
		// TODO: better checking of lengths
		memcpy(&tx_buffer[config::callsign_length], data, transmit_size);
	}

	// Try to transmit the data.
	bool ok = radio->sendPacket((const uint8_t*)tx_buffer, static_cast<uint8_t>(config::callsign_length + transmit_size)); 
	radio->setRXstate();

	if(ok) {
		// It worked, we're ready to transmit again.
		send_string("Sent: %s", tx_buffer);	// TODO: this is eventually unsafe but is fine for now with small packets and constant buffer clearing.
		tx_state = TXState::READY;
		transmit_size = 0;
	} else {
		// We couldn't transmit. Perhaps because we have data to receive?
		// State remains TRANSMITTING.
		send_string("Couldn't transmit, trying again.");
	}
}
