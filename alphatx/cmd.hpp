/*
 * cmd.hpp
 *
 *  Created on: Aug. 8, 2020
 *      Author: Richard
 */

#ifndef CMD_HPP_
#define CMD_HPP_

#include "stm32f1xx_hal.h"

namespace cmd{

/**
 * @brief Indicates to the command system that there is a new command that needs to be processed.
 */
void new_command(const char* cmd_str, uint8_t len);

/**
 * @brief Runs the command processing loop.
 */
void process_command(void);

}
#endif /* CMD_HPP_ */
