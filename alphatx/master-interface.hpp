/*
 * master-interface.h
 *
 *  Created on: January 1, 2021
 *      Author: Richard
 */

#ifndef ALPHATX_MASTER_INTERFACE_H_
#define ALPHATX_MASTER_INTERFACE_H_

#include "stm32f1xx_hal.h"
#include "config.hpp"

#ifdef __cplusplus

/**
 * @brief Master interface. The default master interface will take in bytes
 * over the UART and transmit them over the radio after not receiving any more
 * bytes for a period of 1 second.
 */

class MasterInterface {
    public:
    MasterInterface(const char* name, bool is_debug, UART_HandleTypeDef* uart);

    bool add_byte(uint8_t byte);
    virtual bool poll_for_transmission(void);
    void send_over_uart(uint8_t* data, uint32_t size);

    private:
    /**
     * @brief The UART that this interface should transmit from.
     */
    UART_HandleTypeDef* uart;

    /**
     * @brief The name of this master interface.
     */
    const char* name;

    /**
     * @brief Whether or not this master interface is for debug use, and should
     * therefore reset the UART when it's done processing.
     * TODO: this should really be factored out into a different class.
     */
    bool is_debug;

    /**
     * @brief Size of the receive buffer. This data is buffered and then eventually
     * transmitted over the radio.
     */
    static const uint32_t rx_buf_size = 64;

    /**
     * @brief Buffer for received data.
     */
    uint8_t rx_buf[rx_buf_size];

    /**
     * @brief The index of the next character spot in the RX buffer.
     */
    uint16_t next_char;

    //////////// Below this line can be broken into a different class:

    /**
     * @brief The time of the last received bytes.
     */
    uint32_t last_received_time;
};

extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* ALPHATX_MASTER_INTERFACE_H_ */
