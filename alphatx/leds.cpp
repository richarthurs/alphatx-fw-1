/*
 * leds.cpp
 *
 *  Created on: Aug. 3, 2020
 *      Author: Richard
 */


#include "leds.hpp"
#include "main.h"
#include "stm32f1xx_hal.h"
#include "radio.hpp"

namespace {

/**
 * @brief Colours for the RGB LED.
 */
enum class Colour {
	RED,
	GREEN,
	BLUE,
	YELLOW,
	TEAL,
	PURPLE,
	WHITE,
};

void set_colour(Colour colour);
void leds_off(void);

/**
 * @brief Easier to remember defines for LEDs
 * since the hardware has them configured as
 * active low.
 */
#define LED_ON GPIO_PIN_RESET
#define LED_OFF GPIO_PIN_SET

/**
 * @brief Sets the colour of the RGB LED.
 * @param colour the colour to set.
 */
void set_colour(Colour colour) {
	switch(colour) {
	case Colour::RED:
		HAL_GPIO_WritePin(led_red_n_GPIO_Port, led_red_n_Pin, LED_ON);
		HAL_GPIO_WritePin(led_green_n_GPIO_Port, led_green_n_Pin, LED_OFF);
		HAL_GPIO_WritePin(led_blue_n_GPIO_Port, led_blue_n_Pin, LED_OFF);
		break;
	case Colour::GREEN:
		HAL_GPIO_WritePin(led_red_n_GPIO_Port, led_red_n_Pin, LED_OFF);
		HAL_GPIO_WritePin(led_green_n_GPIO_Port, led_green_n_Pin, LED_ON);
		HAL_GPIO_WritePin(led_blue_n_GPIO_Port, led_blue_n_Pin, LED_OFF);
		break;
	case Colour::BLUE:
		HAL_GPIO_WritePin(led_red_n_GPIO_Port, led_red_n_Pin, LED_OFF);
		HAL_GPIO_WritePin(led_green_n_GPIO_Port, led_green_n_Pin, LED_OFF);
		HAL_GPIO_WritePin(led_blue_n_GPIO_Port, led_blue_n_Pin, LED_ON);
		break;
	case Colour::YELLOW:
		HAL_GPIO_WritePin(led_red_n_GPIO_Port, led_red_n_Pin, LED_ON);
		HAL_GPIO_WritePin(led_green_n_GPIO_Port, led_green_n_Pin, LED_ON);
		HAL_GPIO_WritePin(led_blue_n_GPIO_Port, led_blue_n_Pin, LED_OFF);
		break;
	case Colour::TEAL:
		HAL_GPIO_WritePin(led_red_n_GPIO_Port, led_red_n_Pin, LED_OFF);
		HAL_GPIO_WritePin(led_green_n_GPIO_Port, led_green_n_Pin, LED_ON);
		HAL_GPIO_WritePin(led_blue_n_GPIO_Port, led_blue_n_Pin, LED_ON);
		break;
	case Colour::PURPLE:
		HAL_GPIO_WritePin(led_red_n_GPIO_Port, led_red_n_Pin, LED_ON);
		HAL_GPIO_WritePin(led_green_n_GPIO_Port, led_green_n_Pin, LED_OFF);
		HAL_GPIO_WritePin(led_blue_n_GPIO_Port, led_blue_n_Pin, LED_ON);
		break;
	case Colour::WHITE:
		HAL_GPIO_WritePin(led_red_n_GPIO_Port, led_red_n_Pin, LED_ON);
		HAL_GPIO_WritePin(led_green_n_GPIO_Port, led_green_n_Pin, LED_ON);
		HAL_GPIO_WritePin(led_blue_n_GPIO_Port, led_blue_n_Pin, LED_ON);
		break;
	}
}

/**
 * @brief Turns off all LEDs.
 */
void leds_off(void) {
	HAL_GPIO_WritePin(led_red_n_GPIO_Port, led_red_n_Pin, LED_OFF);
	HAL_GPIO_WritePin(led_green_n_GPIO_Port, led_green_n_Pin, LED_OFF);
	HAL_GPIO_WritePin(led_blue_n_GPIO_Port, led_blue_n_Pin, LED_OFF);
}

}

/**
 * @brief Blinks the LED using the system timer to indicate
 * the RSSI.
 *
 * This should be called from the main loop. If the LED stops
 * blinking, the main loop has stalled.
 */
void led::blink(void) {
  static uint32_t last_led_tick = 0;
  static bool on = false;
  if(HAL_GetTick() - last_led_tick > 500) {
	  if(on) {
		  leds_off();
		  on = false;
	  } else {
		  // Indicate RSSI with the RGB LED.
		  // This range is calibrated for room-scale movements.
		  // The CC1101 is actually sensitive to around -116 dBm,
		  // but we can't get that low over a small distance even
		  // with maximum RX attenuation and lowest transmit power.
		  int16_t rssi = get_last_rssi();
		  if(rssi >= -30) {
			  set_colour(Colour::BLUE);
		  }
		  else if(rssi >= -50) {
			  set_colour(Colour::GREEN);
		  }
		  else if(rssi >= -60) {
			  set_colour(Colour::YELLOW);
		  } else {
			  set_colour(Colour::RED);
		  }

		  on = true;
	  }
	  last_led_tick = HAL_GetTick();
  }
}



