/*
 * isr.cpp
 *
 *  Created on: Aug 3, 2020
 *      Author: Richard
 *
 *  Interrupt service routines.
 */

#include "main.h"
#include "stm32f1xx_hal.h"
#include "flags.hpp"
#include "uart.hpp"
#include "radio.hpp"
#include "global.hpp"

/**
 * @brief EXTI (external GPIO interrupt) handler.
 *
 * Sets the appropriate flags when interrupts on GPIO pins
 * fire.
 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
	if(radio_enabled()) {
		if(GPIO_Pin == rf_gdo0_Pin) {
			flag_rf_gdo0 = true;
		} else {
			while(1);	// Should never get here!
		}
	}
}

/**
 * @brief UART receive complete interrupt. Called when the
 * UART has received the configured number of characters.
 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
	new_char_from_isr(huart);
}



