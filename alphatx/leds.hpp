/*
 * leds.hpp
 *
 *  Created on: Aug. 3, 2020
 *      Author: Richard
 */

#ifndef LEDS_HPP_
#define LEDS_HPP_

namespace led{
void blink(void);
}


#endif /* LEDS_HPP_ */
