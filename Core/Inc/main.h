/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
void init_alpha_main(UART_HandleTypeDef* debug_uart_ptr, UART_HandleTypeDef* master_uart_ptr, SPI_HandleTypeDef *spi_handle);
void alpha_loop(void);
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define led_red_n_Pin GPIO_PIN_0
#define led_red_n_GPIO_Port GPIOA
#define led_green_n_Pin GPIO_PIN_1
#define led_green_n_GPIO_Port GPIOA
#define rf_cs_gpio_n_Pin GPIO_PIN_4
#define rf_cs_gpio_n_GPIO_Port GPIOA
#define rf_clk_Pin GPIO_PIN_5
#define rf_clk_GPIO_Port GPIOA
#define rf_miso_Pin GPIO_PIN_6
#define rf_miso_GPIO_Port GPIOA
#define rf_mosi_Pin GPIO_PIN_7
#define rf_mosi_GPIO_Port GPIOA
#define rf_gdo0_Pin GPIO_PIN_10
#define rf_gdo0_GPIO_Port GPIOB
#define rf_gdo0_EXTI_IRQn EXTI15_10_IRQn
#define led_blue_n_Pin GPIO_PIN_5
#define led_blue_n_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
